# Job application

Job application for frontend developer, done with [AngularJS v1.5.0](https://code.angularjs.org/1.5.0/docs/api) , CSS3 and HTML5.

[![License](https://img.shields.io/packagist/l/doctrine/orm.svg)](https://img.shields.io/packagist/l/doctrine/orm.svg)

## Requirements
- [node v-4.4.5](https://nodejs.org/download/release/v4.4.5/) or higher
- [npm](https://www.npmjs.com/) - installed with node
- [bower](http://bower.io/) - run ```npm install -g bower```
- [gulp](http://gulpjs.com/) - run ```npm install -g gulp```

## How to run

1. Make git clone/pull to your local folder.

1.1 Navigate to the front folder

2. Run ```npm install```

3. Run ```bower install```

4. Run ```gulp```

    * Run ```gulp --prod``` - for production

5. Go to backend folder and follow instruction for setting up node server. After server is setup page will be visible on ```localhost:5000```