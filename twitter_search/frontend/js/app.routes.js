/**
 * Router
 * @namespace Router
 */
(function() {
    'use strict';

    angular.module('app.routes').config(RouterConfig);

    RouterConfig.$inject = ['$stateProvider', '$urlRouterProvider'];

    /**
     * @namespace RouterConfig
     * @name RouterConfig
     * @desc Routing configuration on angular frontend MVC
     * @memberOf Router
     * @param $stateProvider
     * @param $urlRouterProvider
     * @constructor
     */
    function RouterConfig($stateProvider, $urlRouterProvider) {
        $urlRouterProvider.otherwise("/search");

        var getView = function(module) {
            return './js/modules/' + module + '/' + module + '.html';
        };

        var getSubView = function(module, submodule) {
            return './js/modules/' + module + '/' + module + '.' + submodule + '.html';
        };

        $stateProvider
            .state('search', {
                url: '/search',
                views: {
                    'main@': {
                        templateUrl: getView('search'),
                        controller: 'SearchController'
                    }
                }
            });
    }
})();