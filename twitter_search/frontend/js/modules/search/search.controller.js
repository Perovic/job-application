/**
 * Search Controller
 * @namespace Controller
 */
(function() {
    'use strict';

    angular.module('app.controllers').controller('SearchController', SearchController);

    SearchController.$inject = ['$scope', '$location', '$sce', '$anchorScroll', '$mdDialog', 'SocketService', 'TwitterService', 'TermBuilder'];

    function SearchController($scope, $location, $sce, $anchorScroll, $mdDialog, SocketService, TwitterService, TermBuilder) {
        var vm = this;
        vm.currentPage = 1;
        vm.itemsPerPage = 20;
        vm.searchTerm = '';
        vm.searchObject = {
            tContent: '',
            tExact: '',
            tExclude: '',
            fromAcc: '',
            toAcc: '',
            mention: '',
            fromDate: null,
            toDate: null
        };
        vm.isCollapsed = true;
        $scope.selectedIndex = 0;
        $scope.tabs = [];


        vm.addTab = addTab;
        vm.goLive = goLive;
        vm.highlight = highlight;
        $scope.onTabSelected = onTabSelected;
        vm.removeTab = removeTab;
        vm.scrollTo = scrollTo;
        vm.submit = submit;
        vm.twitterSearch = twitterSearch;

        //cleanup work
        $scope.$on('destroy', function(){
            SocketService.removeAllListeners();
        });

        //region Private methods

        function addTab(title, queryObject){
            var tabs = $scope.tabs;
            var term = TermBuilder.build(queryObject);
            var tab = {
                title: title,
                active: true,
                totalItems: 0,
                q: term,
                liveFeed: false
            };
            $scope.selectedIndex++;
            if (!dupes(tab)) {
                tabs.push(tab);
                resetSearchObject();
                vm.tTitle = '';
                twitterSearch(term, tab);
            } else {
                alert('A search with this query already exists');
            }
        }

        function dupes(tab) {
            var tabs = $scope.tabs;
            for (var j = 0; j < tabs.length; j++) {
                if (tab.q == tabs[j].q) {
                    return true;
                }
            }
            return false;
        }

        function goLive(q, tab){
            if(tab.liveFeed){
                spawnSearch(q, tab);
            }
            else {
                unsubscribe(q);
            }
        }

        function highlight(text, search){
            if (!search) {
                return $sce.trustAsHtml(text);
            }
            return $sce.trustAsHtml(text.replace(new RegExp(search, 'gi'), '<span class="highlightedText"><b>$&</b></span>'));
        }

        function onTabSelected(tab) {
            $scope.selectedIndex = this.$index;
            updateScope(tab);

        }

        function openModal(title, content){
            $mdDialog.show($mdDialog.alert()
                .parent(angular.element(document.body))
                .title(title)
                .ok("Ok")
                .clickOutsideToClose(true)
                .content(content)
            )
        }

        function removeTab(tab){
            var tabs = $scope.tabs;
            for (var j = 0; j < tabs.length; j++) {
                if (tab.title == tabs[j].title) {
                    $scope.selectedIndex = (j == 0 ? 1 : j - 1);
                    updateScope(tabs[j === 0 ? 1 : j - 1]);
                    tabs.splice(j, 1);
                    break;
                }
            }
        }

        function resetSearchObject() {
            vm.searchObject = {
                tContent: '',
                tExact: '',
                tExclude: '',
                fromAcc: '',
                toAcc: '',
                mention: '',
                fromDate: null,
                toDate: null
            };
        }

        function scrollTo(id){
            $location.hash(id);
            $anchorScroll();
        }

        function spawnSearch(q, tab) {
            SocketService.emit('q', q);
            SocketService.on('tweet_' + q, function (tweet) {
                if (vm['tweets_' + q].length > 500) {
                    vm['tweets_' + q].shift();
                }
                var tweets = [];
                tweets.push(tweet);
                vm['tweets_' + q] = tweets.concat(vm['tweets_' + q]);
                tab.totalItems++;

                updateScope(tab)
            });
        }

        function submit($event){
            if ($event.which !== 13) return;
            if (vm.tTitle) {
                vm.addTab(vm.tTitle, vm.tContent);
            }
        }

        function twitterSearch(term, tab){
            TwitterService.search(term).then(function(response){
                var data = response.data;

                vm.searchTerm = term;
                vm['tweets_' + term] = [];
                vm['tweets_' + term] = data.statuses;
                tab.totalItems = data.statuses ? data.statuses.length : 0;
                if(tab.totalItems === 0){
                    openModal('Nothing found', "Unfortunately there are no search results for " + tab.q + " you can still turn on live feed for possible results.");
                }
                updateScope(tab);
            }, function(error){
                openModal('Error occurred!', error.message);
            });
        }

        function unsubscribe(q){
            SocketService.emit('remove', q);
        }

        function updateScope(tab) {
            if ($scope.tabs[$scope.selectedIndex] && $scope.tabs[$scope.selectedIndex].q == tab.q) {
                vm.tweets = vm['tweets_' + tab.q];
            }
        }

        //endregion
    }
})();