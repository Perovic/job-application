/**
 * Initialization
 * @desc Initializes angular global modules and their dependencies
 * @namespace Initialization
 */
(function(){
    'use strict';

    angular.module('app',[
        'ngMaterial',
        'app.config',
        'app.constants',
        'app.filters',
        'app.routes',
        'app.services',
        'app.directives',
        'app.controllers'
    ]);

    angular.module('app.routes', ['ui.router']);

    angular.module('app.config', []);

    angular.module('app.constants', []);

    angular.module('app.services', ['btford.socket-io']);

    angular.module('app.controllers', ['ui.bootstrap']);

    angular.module('app.directives', []);

    angular.module('app.filters', []);

})();
