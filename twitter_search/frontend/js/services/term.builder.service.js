(function() {
    "use strict";

    angular.module('app.services').factory('TermBuilder', TermBuilder);

    TermBuilder.$inject = [];

    function TermBuilder() {
        var builder = {
            content: function(term, content){
                if (!term) return '';
                var start = content ? ' ' : '';
                return start + term;
            },
            exact: function (term, content) {
                if (!term) return '';
                var start = content ? ' "' : '"';
                return start + term + '"';
            },
            exclude: function (term, content) {
                if (!term) return '';
                var start = content ? ' -' : '-';
                return start + term.split(' ').join(' -');
            },
            from: function (term, content) {
                if (!term) return '';
                var start = content ? ' from:' : 'from:';
                return start + term.split(' ').join(' from:');
            },
            to: function (term, content) {
                if (!term) return '';
                var start = content ? ' to:' : 'to:';
                return start + term.split(' ').join(' to:');
            },
            mention: function (term, content) {
                if (!term) return '';
                var start = content ? ' @' : '@';
                return start + term.split(' ').join(' @');
            },
            fromDate: function (term, content) {
                if (!term) return '';
                var start = content ? ' since:' : 'since:';
                return start + formatDate(term);
            },
            toDate: function (term, content) {
                if (!term) return '';
                var start = content ? ' until:' : 'until:';
                return start + formatDate(term);
            }
        };

        var service = {
            build: function (term) {
                var content = '';
                content += builder.content(term.tContent, content);
                content += builder.exact(term.tExact, content);
                content += builder.exclude(term.tExclude, content);
                content += builder.from(term.fromAcc, content);
                content += builder.to(term.toAcc, content);
                content += builder.mention(term.mention, content);
                content += builder.fromDate(term.fromDate, content);
                content += builder.toDate(term.toDate, content);
                return content
            }
        };

        function formatDate(date) {
            var d = new Date(date),
                month = '' + (d.getMonth() + 1),
                day = '' + d.getDate(),
                year = d.getFullYear();

            if (month.length < 2) month = '0' + month;
            if (day.length < 2) day = '0' + day;

            return [year, month, day].join('-');
        }

        return service;
    }

})();
