(function() {
    "use strict";

    angular.module('app.services').factory('SocketService', SocketService);

    SocketService.$inject = ['socketFactory'];

    function SocketService(socketFactory) {
        var service = socketFactory({
            ioSocket: io.connect('http://localhost:5000')
        });

        return service;
    }

})();
