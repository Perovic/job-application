(function() {
    "use strict";

    angular.module('app.services').factory('TwitterService', TwitterService);

    TwitterService.$inject = ['$http'];

    function TwitterService($http) {
        var service = {
            search: function(term){
                return $http.get('/api/search', {params: {searchTerm: term}});
            }
        };

        return service;
    }

})();
