/**
 * ENDPOINTS
 * @namespace Constants
 */
(function(){
    'use strict';

    /**
     * @namespace EndpointsConstant
     * @name EndpointsConstant
     * @desc Constant object with api endpoints
     * @memberOf Constants
     */
    angular.module('app.constants').constant('ENDPOINTS', {
        'searchAPI': {
            'endpoint': 'localhost:5000/api'
        }
    });

})();