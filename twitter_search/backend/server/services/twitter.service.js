var Twit = require('twit');
var config = require('../../config/config');

var TwitterService = new Twit({
    consumer_key: config.TWITTER_API_KEY,
    consumer_secret: config.TWITTER_API_SECRET,
    access_token: config.TWITTER_ACCESS_TOKEN,
    access_token_secret: config.TWITTER_ACCESS_SECRET
});

module.exports = TwitterService;