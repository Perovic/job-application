var bodyParser = require('body-parser');
var session = require('express-session');
var cookieParser = require('cookie-parser');
var favicon = require('serve-favicon');
var morgan = require('morgan');
var config = require('../../config/config');
var path = require('path');
var twitterService = require('./../services/twitter.service');

module.exports = function(app){
    app.engine('html', require('ejs').renderFile);
    app.set('view engine', 'html');


    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({ extended: false }));

    app.use(morgan('dev'));

    app.use(cookieParser());

    app.set('twitter_service', twitterService);
};