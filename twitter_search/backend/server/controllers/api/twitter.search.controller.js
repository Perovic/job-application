exports.search = function(request, response){
    var twitterService = request.app.get('twitter_service');
    var term = request.query.searchTerm;

    twitterService.get('search/tweets', {q: term, count: 200}, function(error, data, res){
        response.send(data);
    });
};