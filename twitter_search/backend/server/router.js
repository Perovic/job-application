var path = require('path');
var twitterRouter = require('./routers/api/twitter.router');

module.exports = function (app) {

    app.use('/api/search', twitterRouter);

    app.route('/*')
        .get(function(req, res) {
            res.sendFile(path.resolve(app.get('views') + '/index.html'));
        });

    app.get('*', function(req, res) {
        res.redirect('/#' + req.originalUrl);
    });
};