'use strict';
var express = require('express');
var router = express.Router();
var searchController = require('../../controllers/api/twitter.search.controller');

router.get('/', searchController.search);

module.exports = router;