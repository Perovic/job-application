module.exports = function(app, server){
    var io = require('socket.io')(server);

    var searches = {};

    io.on('connection', function(socket) {
        searches[socket.id] = {};

        require('./controllers/api/twitter.socket.controller')(app, socket, searches);

    });
};